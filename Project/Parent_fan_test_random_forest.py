#!/usr/bin/env python
# coding: utf-8

# In[1]:


import time
notebookstart= time.time()

import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
# Visualization
import seaborn as sns
import matplotlib.pyplot as plt
import os


# In[2]:


train_df = pd.read_json('train.json')
test_df = pd.read_json('test.json')
df_index = train_df['id']
y = train_df.cuisine.copy()
train_df.head()


# In[3]:


for _, x in train_df.iterrows():
    for ingredient in x.ingredients:
        ingredient = ingredient.replace(',', '')
for _, x in test_df.iterrows():
    for ingredient in x.ingredients:
        ingredient = ingredient.replace(',', '')


# In[4]:


from sklearn.feature_extraction.text import CountVectorizer
vect = CountVectorizer(max_features = 3000, tokenizer=lambda x: [i.strip() for i in x.split(',')], lowercase=False)
#vect = CountVectorizer(tokenizer=lambda x: [i.strip() for i in x.split(',')], lowercase=False)
x = vect.fit_transform(train_df['ingredients'].apply(','.join)) 
#word_vector_df = pd.DataFrame(x.todense(),columns=vect.get_feature_names())
x_test = vect.transform(test_df['ingredients'].apply(','.join)) 


# In[64]:


feature_name = vect.get_feature_names()
print((x[0,:] + x[1,:]).shape)


# In[98]:


def getChildren(feature_name):
    Children=[]
    for i in feature_name:
        if ' ' not in i:
            if i != '' and i != ' ':
                Children.append(i)
    return Children


# In[115]:


def UpdateVector1(data, feature_name):
    children=getChildren(feature_name)
    #print(len(children))
    column=feature_name
    deletion=[]
    for i in children:
        #print(children.index(i))
        for j in column:
            if i!=j and i in j:
                data[:,feature_name.index(i)]=data[:,feature_name.index(i)] + data[:,feature_name.index(j)]
                for idx in range(data[:,feature_name.index(i)].shape[1]):
                    if data[idx,feature_name.index(i)]>1:
                        data[idx,feature_name.index(i)]=1
                deletion.append(feature_name.index(j))
    deletion.sort(reverse = True)
    data = data.todense()
    data = np.delete(data,deletion,axis = 1)
    #for i in range(data.shape[0]):
    #    for j in deletion:
    #        del data[i,j]
    return data


# In[110]:


x_ = UpdateVector1(x.copy(),feature_name)


# In[111]:


print(x_.shape)


# In[116]:


x_test_ = UpdateVector1(x_test.copy(),feature_name)


# In[117]:


split = int(0.8 * x.shape[0])  # Use 80% of the data for training and 20% for validation.
print(f'Training, validation split index: {split}')


# In[120]:


X_train, y_train, X_valid, y_valid = x_[:split], y[:split], x_[split:], y[split:]


# In[121]:


from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score


# In[122]:


model = LogisticRegression(penalty='l2', C = 1.2)
model.fit(X_train, y_train)


# In[123]:


train_preds = model.predict(X_train)
valid_preds = model.predict(X_valid)

print(f'Training classification accuracy: {accuracy_score(y_train, train_preds)}')
print(f'Validation classification accuracy: {accuracy_score(y_valid, valid_preds)}')


# In[124]:


model.fit(x_, y)


# In[125]:


preds = model.predict(x_)

print(f'Training classification accuracy: {accuracy_score(y, preds)}')


# In[126]:


test_preds = model.predict(x_test_)


# In[127]:


submission = pd.DataFrame()
submission['id'] = test_df.id
submission['cuisine'] = pd.Series(test_preds)


# In[128]:


submission.head()


# In[129]:


submission.to_csv('predictions.csv', index=False)


# In[ ]:




