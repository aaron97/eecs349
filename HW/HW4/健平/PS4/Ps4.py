import numpy as np
import math

def cosine_similarity(x, y):
	if len(x) != len(y): return 0
	xy = 0
	x2 = 0
	y2 = 0
	for i in range(len(x)):
		xy += x[i]*y[i]
		x2 += x[i]**2
		y2 += y[i]**2

	return xy/(math.sqrt(x2)*math.sqrt(y2))

#a = [1,2]
#b = [2,4]
#a = np.array(a)
#b = np.array(b)
#print(cosine_similarity(a, b))

#============================================================

import numpy as np
import json
import seaborn as sns
import os

with open("dataset.json",'r') as f:
    load_dict = json.load(f)
test = load_dict['test']
images = load_dict['images']
train = load_dict['train']
captions = load_dict['captions']

vgg = np.load('vgg_rep.npy')
pixel = np.load('pixel_rep.npy')

test_index = []
train_index = []
for img in test:
    test_index.append(images.index(img))
for img in train:
    train_index.append(images.index(img))

f = open('vgg.txt', 'w')
for idx in test_index:
    similarity = 0
    index = train_index[0]
    test_feature = vgg[idx]
    for idx_train in train_index:
        train_feature = vgg[idx_train]
        similarity_tmp = cosine_similarity(test_feature,train_feature)
        if similarity_tmp > similarity:
            similarity = similarity_tmp
            index = idx_train
    f.write(captions[images[index]])
    f.write('\n')
f.close()

f = open('pixel.txt', 'w')
for idx in test_index:
    similarity = 0
    index = train_index[0]
    test_feature = pixel[idx]
    for idx_train in train_index:
        train_feature = pixel[idx_train]
        similarity_tmp = cosine_similarity(test_feature,train_feature)
        if similarity_tmp > similarity:
            similarity = similarity_tmp
            index = idx_train
    f.write(captions[images[index]])
    f.write('\n')
f.close()



