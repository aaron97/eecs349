import numpy as np
import json

def cosine_similarity(vector_a, vector_b):
    """
    :param vector_a: vector a
    :param vector_b: vector b
    :return: sim
    """
    vector_a = np.mat(vector_a)
    vector_b = np.mat(vector_b)
    num = float(vector_a * vector_b.T)
    denom = np.linalg.norm(vector_a) * np.linalg.norm(vector_b)
    cos = num / denom
    sim = 0.5 + 0.5 * cos
    return sim

def question_6():

    with open('cnn_dataset.json') as data_file:
        data = json.load(data_file)
    
    vgg_mj1 = data['vgg_rep']['mj1']
    vgg_mj2 = data['vgg_rep']['mj2']
    vgg_cat = data['vgg_rep']['cat']
    pix_mj1 = data['pixel_rep']['mj1']
    pix_mj2 = data['pixel_rep']['mj2']
    pix_cat = data['pixel_rep']['cat']

    v_12 = cosine_similarity(vgg_mj1,vgg_mj2)
    v_1c = cosine_similarity(vgg_mj1,vgg_cat)
    v_2c = cosine_similarity(vgg_mj2,vgg_cat)
    p_12 = cosine_similarity(pix_mj1,pix_mj2)
    p_1c = cosine_similarity(pix_mj1,pix_cat)
    p_2c = cosine_similarity(pix_mj2,pix_cat)

    print(v_12, v_1c, v_2c, p_12, p_1c, p_2c)

def question_8():

    with open('dataset.json') as data_file:
        data = json.load(data_file)

    vgg = np.load('vgg_rep.npy')
    pixel = np.load('pixel_rep.npy')

    train = data['train']
    test = data['test']
    images = data['images']
    captions = data['captions']

    test_data_vgg = []
    test_data_pix = []
    test_name = []
    train_data_vgg = []
    train_data_pix = []
    train_name = []
    train_captions = []

    # test data
    for t in test:
        test_name.append(t)
        index = images.index(t)
        test_data_vgg.append(vgg[index])
        test_data_pix.append(pixel[index])

    # train data
    for t in train:
        train_name.append(t)
        index = images.index(t)
        train_data_vgg.append(vgg[index])
        train_data_pix.append(pixel[index])
        train_captions.append(captions[t])

    f1 = open('vgg.txt', 'w')
    f2 = open('pixel.txt', 'w')
    for i in range(len(test_name)):
        sim_vgg = []
        sim_pix = []
        for j in range(len(train_name)):
            sim_vgg.append(cosine_similarity(test_data_vgg[i], train_data_vgg[j]))
            sim_pix.append(cosine_similarity(test_data_pix[i], train_data_pix[j]))
        vgg_max_index = sim_vgg.index(max(sim_vgg))
        print(vgg_max_index)
        pix_max_index = sim_pix.index(max(sim_pix))
        vgg_cap = train_captions[vgg_max_index]
        pix_cap = train_captions[pix_max_index]
        vgg_train_name = train_name[vgg_max_index]
        pix_train_name = train_name[pix_max_index]
        testname = test_name[i]

        f1.write(vgg_cap + ' ' + vgg_train_name + ' ' + testname + '\n')
        f2.write(pix_cap + ' ' + pix_train_name + ' ' + testname + '\n')

    f1.close()
    f2.close()

question_8()